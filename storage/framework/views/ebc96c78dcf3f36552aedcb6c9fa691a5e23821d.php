<?php $__env->startPush('stylesheets'); ?>
<?php echo $__env->make('includes/dynamic-table-css', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopPush(); ?>

<?php $__env->startSection('main_container'); ?>
    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                    <h3>Subject Management<small></small></h3>
                </div>
                <div class="title_right">
                </div>
            </div>

            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <div class="nav navbar-left add-button">
                            <a href="<?php echo e(route('subject.create')); ?>" class="btn btn-success"><i class="fa fa-plus" aria-hidden="true"></i> Add Subject</a>
                        </div>
                        
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <table id="datatable-checkbox" class="table table-striped table-bordered bulk_action">
                            <thead>
                            <tr>
                                
                                <th>Sl.No</th>
                                <th>Subject Name</th>
                                
                                <th>Created</th>
                                <th>Updated</th>
                                <th class="column-title no-link last"><span class="nobr">Action</span>
                                </th>
                            </tr>
                            </thead>

                            <tbody>
                            <?php $__currentLoopData = $subjects; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $index => $subject): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <tr>
                                    
                                    <td><?php echo e($index + 1); ?></td>
                                    <td><?php echo e($subject->name); ?></td>
                                    
                                    <td><?php echo e($subject->created_at); ?></td>
                                    <td><?php echo e($subject->updated_at); ?></td>
                                    <td class=" last">
                                        <a href="<?php echo e(route('subject.edit', ['id' => $subject->id])); ?>" class="btn btn-xs btn-warning waves-effect waves-light" data-toggle="tooltip" data-placement="left" title="Edit Subject"><i class="fa fa-pencil-square fa-lg" aria-hidden="true"></i> Edit</a>
                                        <a href="<?php echo e(route('subject.delete', ['id' => $subject->id])); ?>" class="btn btn-xs btn-danger waves-effect waves-light" data-toggle="tooltip" data-placement="top" title="Delete Subject"><i class="fa fa-trash fa-lg" aria-hidden="true"></i> Delete</a>
                                        
                                    </td>
                                </tr>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php $__env->startPush('scripts'); ?>
<?php echo $__env->make('includes/dynamic-table-scripts', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<script src="<?php echo e(asset("school_class")); ?>"></script>
<?php $__env->stopPush(); ?>


<?php echo $__env->make('layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>