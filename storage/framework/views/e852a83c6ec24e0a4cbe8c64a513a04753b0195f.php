<?php $__env->startPush('stylesheets'); ?>
<!-- Example -->
<!--<link href=" <link href="<?php echo e(asset("css/myFile.min.css")); ?>" rel="stylesheet">" rel="stylesheet">-->
<?php $__env->stopPush(); ?>

<?php $__env->startSection('main_container'); ?>

    <div class="right_col" role="main">
        <div class="">

            <div class="page-title">
                <div class="title_left">
                    <h3>Edit School</h3>
                </div>

                
                
                
                
                
                
                
                
                
                
                
            </div>

            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Form to edit a School</h2>
                            
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <br />
                            <form method = "POST" action="<?php echo e(route('school.update')); ?>" id="demo-form2" data-parsley-validate class="form-horizontal form-label-left">
                                <?php echo e(csrf_field()); ?>


                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="school_code">School Code <span class="required">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" id="school_code" name="school_code" value="<?php echo e($school->school_code); ?>" required="required" class="form-control col-md-7 col-xs-12">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="school_name">School Name <span class="required">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" id="school_name" name="school_name" value="<?php echo e($school->name); ?>" required="required" class="form-control col-md-7 col-xs-12">
                                    </div>
                                </div>

                                
                                    
                                    
                                    
                                        

                                            
                                                
                                                    
                                                
                                                    
                                                
                                            
                                        
                                    
                                

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">School Level<span class="required">*</label>
                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                        <?php echo Form::select('school_level_id', (['0' => '---Select School Level---'] + $schoolLevels), $selectedSchoolLevel, ['class' => 'form-control']); ?>

                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Dzongkhag /Thromde <span class="required">*</label>
                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                        <?php echo Form::select('dzongkhag_id', (['0' => '---Select Dzongkhag---'] + $dzongkhags), $selectedDzongkhag, ['class' => 'form-control']); ?>

                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">School Status <span class="required">*</label>
                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                        <?php echo Form::select('school_status_type_id', (['0' => '---Select School Status---'] + $schoolStatusTypes), $selectedSchoolStatusType, ['class' => 'form-control']); ?>

                                    </div>
                                </div>

                                <div class="ln_solid"></div>
                                <div class="form-group">
                                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                        <input type="hidden" name="id" value="<?php echo e($schoolId); ?>">
                                        <input type="hidden" name="version" value="<?php echo e($school->version); ?>"/>
                                        <a href="<?php echo e(route('school.index')); ?>" class="btn btn-primary" type="button">Cancel</a>
                                        <button type="submit" class="btn btn-success">Update</button>
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>