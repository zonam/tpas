<?php $__env->startPush('stylesheets'); ?>
<?php echo $__env->make('includes/dynamic-table-css', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopPush(); ?>

<?php $__env->startSection('main_container'); ?>
    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                    <h3>School Management<small></small></h3>
                </div>
                <div class="title_right">
                </div>
            </div>

            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <div class="nav navbar-left add-button">
                            <a href="<?php echo e(route('school.create')); ?>" class="btn btn-success"><i class="fa fa-plus" aria-hidden="true"></i> Add School</a>
                        </div>
                        
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <table id="datatable-checkbox" class="table table-striped table-bordered bulk_action">
                            <thead>
                            <tr>
                                <th>Sl.No</th>
                                <th>School Code</th>
                                <th>School Name</th>
                                <th>School Level</th>
                                <th>Dzongkhag / Thromde</th>
                                <th>School Status</th>
                                <th class="column-title no-link last"><span class="nobr">Action</span>
                                </th>
                            </tr>
                            </thead>

                            <tbody>
                            <?php $__currentLoopData = $schools; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $index => $school): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <tr>
                                    <td><?php echo e($index + 1); ?></td>
                                    <td><?php echo e($school->school_code); ?></td>
                                    <td><?php echo e($school->name); ?></td>
                                    <td><?php echo e($school->schoolLevel->name); ?></td>
                                    <td><?php echo e($school->dzongkhag->name); ?></td>
                                    <td><?php echo e($school->schoolStatusType->name); ?></td>
                                    <td class=" last">
                                        <a href="<?php echo e(route('school.edit', ['id' => $school->id])); ?>" class="btn btn-xs btn-warning waves-effect waves-light" data-toggle="tooltip" data-placement="left" title="Edit School"><i class="fa fa-pencil-square fa-lg" aria-hidden="true"></i> Edit</a>
                                        <a href="<?php echo e(route('school.delete', ['id' => $school->id])); ?>" class="btn btn-xs btn-danger waves-effect waves-light" data-toggle="tooltip" data-placement="top" title="Delete School"><i class="fa fa-trash fa-lg" aria-hidden="true"></i> Delete</a>
                                        
                                    </td>
                                </tr>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php $__env->startPush('scripts'); ?>
<?php echo $__env->make('includes/dynamic-table-scripts', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<script src="<?php echo e(asset("school_class")); ?>"></script>
<?php $__env->stopPush(); ?>


<?php echo $__env->make('layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>