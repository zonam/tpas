<?php $__env->startPush('stylesheets'); ?>
<?php echo $__env->make('includes/dynamic-table-css', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopPush(); ?>


<?php $__env->startSection('main_container'); ?>
    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                    <h3>Role Management<small></small></h3>
                </div>
                <div class="title_right">
                </div>
            </div>

            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <div class="nav navbar-left add-button">
                            <a href="<?php echo e(url('/roles/create')); ?>" class="btn btn-success"><i class="fa fa-plus" aria-hidden="true"></i> Add Role</a>
                        </div>
                       
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <table id="datatable-checkbox" class="table table-striped table-bordered bulk_action">
                            <thead>
                            <tr>
                                <th>Role ID</th>
                                <th>Role Name</th>
                                <th>Role Label</th>
                                
                                <th class="column-title no-link last"><span class="nobr">Action</span>
                                </th>
                            </tr>
                            </thead>

                            <tbody>
                            <?php $__currentLoopData = $roles; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $role): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <tr>
                                    <td><?php echo e($role->id); ?></td>
                                    <td><?php echo e($role->name); ?></td>
                                    <td><?php echo e($role->label); ?></td>
                                  
                                    <td class=" last">
                                        
                                         <a href="<?php echo e(url('/')); ?>/roles/view/<?php echo e($role->id); ?>" class="btn btn-xs btn-primary waves-effect waves-light" data-toggle="tooltip" data-placement="left" title="View role"><i class="fa fa-pencil-square fa-lg" aria-hidden="true"></i> View</a>

                                        <a href="<?php echo e(url('/')); ?>/roles/edit/<?php echo e($role->id); ?>" class="btn btn-xs btn-warning waves-effect waves-light" data-toggle="tooltip" data-placement="left" title="Edit role"><i class="fa fa-pencil-square fa-lg" aria-hidden="true"></i> Edit</a>
                                       
                                        
                                       

                                                <!--<div class="modal fade bs-example-modal-sm" id="modal-delete-<?php echo e($role->id); ?>" tabIndex="-1" aria-hidden="true" role="dialog">  
                                                    <div class="modal-dialog modal-sm">
                                                      <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h4 class="modal-title">Confirm Delete</h4>
                                                        </div>
                                                        <div class="modal-body">
                                                            <p>Are you sure you want to delete the role?</p>
                                                        </div>
                                                        <div class="modal-footer">

                                                                <form action="<?php echo e(url('/')); ?>/roles/<?php echo e($role->id); ?>/delete" method="post">
                                                                 <?php echo e(csrf_field()); ?>

                                                                 <?php echo e(method_field('DELETE')); ?>

                                                                 <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                                                    <Button class="btn btn-xs btn-danger waves-effect waves-light" data-toggle="tooltip" type="submit"><i class="fa fa-trash fa-lg"></i> Delete</Button>
                                                                </form>
                                                        </div>
                                                      </div>
                                                  </div>
                                       </div>!-->
                                            <div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-hidden="true" >
                                            <div class="modal-dialog modal-sm">
                                              <div class="modal-content">

                                                <div class="modal-header">
                                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                                                  </button>
                                                  <h4 class="modal-title" id="myModalLabel2">Modal title</h4>
                                                </div>
                                                <div class="modal-body">
                                                  <h4>Text in a modal</h4>
                                                  <p>Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor.</p>
                                                  <p>Aenean lacinia bibendum nulla sed consectetur. Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Donec sed odio dui. Donec ullamcorper nulla non metus auctor fringilla.</p>
                                                </div>
                                                <div class="modal-footer">
                                                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                  <button type="button" class="btn btn-primary">Save changes</button>
                                                </div>

                                              </div>
                                            </div>
                                         </div>
                                       
                                    </td>
                                </tr>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php $__env->startPush('scripts'); ?>
<?php echo $__env->make('includes/dynamic-table-scripts', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopPush(); ?>


<?php echo $__env->make('layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>